let canvas = document.querySelector("canvas");
let context = canvas.getContext('2d');


const scale = 0.85;
const width = 74;
const height = 118;
const scaledWidth = scale * width;
const scaledHeight = scale * height;
const walkLoop = [0, 1, 0, 2];
const frameLimit = 7;

const DIAMOND_DIMINTION = 60;

let currentLoopIndex = 0;
let frameCount = 0;
let frameCount2 = 0;

let currentDirection = 0;
let currentLoopIndex2 = 0;

let speed = 3;
let spacePushed = false;
let keyDown = false;
let keyPressed = "default"

let qPressed = false;
let enemyMoveUp = false;
let fireAtEdge = false;
let enemyDirection = 0;
const score = 0;
let scoreCount = 0;
if (score) {
    scoreCount = score;
}


let diamondTwoX = 100;
let diamondTwoY = 359;
let diamondOneX = 666;
let diamondOneY = 185;
let diamond1_visable = true;
let diamond2_visable = true;




let gamesEnd = false;
let gameWins = false;
let scoredPoint = false;





let character = new Image();
character.src = "assets/img/knight.png";

let diamondCollect = new Image();
diamondCollect.src = "assets/img/diamond.png";

let dragon = new Image();
dragon.src = "assets/img/dragon.png";

let fireImg = new Image();
fireImg.src = "assets/img/fire.png";

let background = new Image();
background.src = "assets/img/BG3.jpg"

let endScreen = new Image();
endScreen.src = "assets/img/gameover.jpg";

let princessImg = new Image();
princessImg.src = "assets/img/princess.png";

let winScreen = new Image();
winScreen.src = "assets/img/win.png";


function GameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.mvmtDirection = "None";
}

let player = new GameObject(character, 352, 123, 323, 290);
let diamond = new GameObject(diamondCollect, 610, 337, 10, 37);
let enemy = new GameObject(dragon, 680, 0, 150, 150);
let fire = new GameObject(fireImg, 693, 60, 30, 30);
let fire2 = new GameObject(fireImg, 693, 60, 30, 30);
let bg = new GameObject(background, 0, 0, 800, 500);
let gameOver = new GameObject(endScreen, 0, 0, 800, 500);
let princess = new GameObject(princessImg, 0, 0, 100, 100);
let win = new GameObject(winScreen, 0, 0, 800, 500);

// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

//Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input

function input(event) {
    // Take Input from the Player

    if (event.type === "keydown") {
        switch (event.keyCode) {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break; //Left key
            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break; //Up key
            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break; //Right key
            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break; //Down key
            case 13: //enter key
                gamerInput = new GamerInput("Enter");
                break;
            case 32:
                gamerInput = new GamerInput("Spacebar");
                break;
            case 81: //Q key
                gamerInput = new GamerInput("qKey");
                break;
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None");

    }
}


function update() {

    if (gamerInput.action === "Up") {
        console.log("Move Up");
        currentDirection = 3;
        keyPressed = "up";
        keyDown = true;
    } else if (gamerInput.action === "Down") {
        console.log("Move Down");
        currentDirection = 0;
        keyPressed = "down";
        keyDown = true;
    } else if (gamerInput.action === "Left") {
        console.log("Move Left");
        currentDirection = 1;
        keyPressed = "left";
        keyDown = true;
    } else if (gamerInput.action === "Right") {
        console.log("Move Right");
        currentDirection = 2;
        keyPressed = "right";
        keyDown = true;
    } else if (gamerInput.action === "qKey") {
        console.log("q key pressed");
        qPressed = "true";
    } else if (gamerInput.action === "sKey") {
        console.log("s key pressed");
        sPressed = "true";
    }

    console.log(player.x, "XXX-----")
    console.log(player.y, "YYY-----")

}



function movePlayer() {
    if (keyPressed == "up") {
        player.y -= speed; // Move Player Up
        keyPressed = "default";
    } else if (keyPressed == "down") {
        player.y += speed; // Move Player Down
        keyPressed = "default";
    } else if (keyPressed == "left") {
        player.x -= speed; // Move Player Left
        keyPressed = "default";
    } else if (keyPressed == "right") {
        player.x += speed; // Move Player Right
        keyPressed = "default";
    }

    if (qPressed == "true") {
        speed = 4;
        qPressed = "false";
    }


}




var dynamic = nipplejs.create({
    color: 'grey',
    zone: document.getElementById("canvas")

});


dynamic.on('added', function(evt, nipple) {
    //nipple.on('start move end dir plain', function (evt) {
    nipple.on('dir:up', function(evt, data) {
        //console.log("direction up");
        gamerInput = new GamerInput("Up");
    });
    nipple.on('dir:down', function(evt, data) {
        //console.log("direction down");
        gamerInput = new GamerInput("Down");
    });
    nipple.on('dir:left', function(evt, data) {
        //console.log("direction left");
        gamerInput = new GamerInput("Left");
    });
    nipple.on('dir:right', function(evt, data) {
        //console.log("direction right");
        gamerInput = new GamerInput("Right");
    });
    nipple.on('end', function(evt, data) {
        //console.log("mvmt stopped");
        gamerInput = new GamerInput("None");
    });
});


function writeScore() {

    let scoreString = "score: " + scoreCount;
    context.font = '22px sans-serif';
    context.fillStyle = "red";
    context.fillText(scoreString, 690, 60)
}



function drawFrame(image, frameX, frameY, canvasX, canvasY) {
    context.drawImage(image,
        frameX * width, frameY * height, width, height,
        canvasX, canvasY, scaledWidth, scaledHeight);
}

function animate() {
    if (gamerInput.action != "None") {
        frameCount++;
        if (frameCount >= frameLimit) {
            frameCount = 0;
            currentLoopIndex++;
            if (currentLoopIndex >= walkLoop.length) {
                currentLoopIndex = 0;
            }
        }
    } else {
        currentLoopIndex = 0;
    }

    drawFrame(player.spritesheet, walkLoop[currentLoopIndex], currentDirection, player.x, player.y);
}

function moveEnemy() {
    if (enemyMoveUp == true) {
        enemy.y--;

    } else {
        enemy.y++;
        // speed = 4;
    }

    if (enemy.y >= 450) {
        enemy.y = 450;
        enemyMoveUp = true;
    } else if (enemy.y <= 0) {
        enemy.y = 1;
        enemyMoveUp = false;
    }
}

function fireShoot() {

    fire.x = fire.x - 10;
    if (fire.x <= 0) {
        fire.y = enemy.y + 50;
        fire.x = enemy.x;
    }

    fire2.x = fire2.x - 3;
    if (fire2.x <= 0) {
        fire2.y = enemy.y + 50;
        fire2.x = enemy.x;
    }

}


// collision between the player and fire 
function collision() {
    if (fire.x < player.x + (width - 50) && fire.x + (width - 50) > player.x &&
        fire.y < player.y + (height - 80) && fire.y + (height - 80) > player.y) {
        console.log("collision");
        gamesEnd = true;
    }
}

// collision with the door only if the 2 diamonds collected 
function collisionDoor() {

    if (!diamond1_visable && !diamond2_visable) {
        if (player.x >= 329 && player.x <= 389) {
            if (player.y >= 0 && player.y <= 76) {

                gameWins = true;
            }
        }
    }
}



function draw() {

    if (gameWins) {
        context.drawImage(win.spritesheet, win.x, win.y, 800, 500);
        // cancelAnimationFrame(gameloop);
    } else if (gamesEnd) {
        context.drawImage(gameOver.spritesheet, gameOver.x, gameOver.y, 800, 500);
    } else {

        context.clearRect(0, 0, canvas.width, canvas.height);
        context.drawImage(bg.spritesheet, bg.x, bg.y, 800, 500);

        drawHealthbar();

        context.drawImage(princess.spritesheet, princess.x, princess.y, 100, 100);
        context.drawImage(fire.spritesheet, fire.x, fire.y, 30, 30);
        context.drawImage(fire2.spritesheet, fire2.x, fire2.y, 30, 30);

        context.drawImage(enemy.spritesheet, enemy.x, enemy.y, 150, 150);

        if (diamond1_visable) {
            context.drawImage(diamondCollect, diamondOneX, diamondOneY, 55, 55);

        }
        if (diamond2_visable) {
            context.drawImage(diamondCollect, diamondTwoX, diamondTwoY, 60, 60);
        }

        animate();
        writeScore();

        collision();
        collisionDoor();
    }
}


var fillVal = 0;

function drawHealthbar() {
    var width = 1100;
    var height = 25;
    var max = 100;
    var val = 10;

    // Draw the background
    context.fillStyle = "#000000";
    // context.clearRect(0, 0, canvas.width, canvas.height);
    context.fillRect(0, 0, width, height);

    // Draw the fill
    context.fillStyle = "#00FF00";
    context.fillRect(0, 0, fillVal * width, height);
    fillVal += 0.0005;

    if (fillVal >= 0.7) {
        gamesEnd = true;
    } else {

        if ((player.x >= diamondOneX && player.x <= diamondOneX + DIAMOND_DIMINTION) &&
            (player.y >= diamondOneY && player.y <= diamondOneY + DIAMOND_DIMINTION) &&
            diamond1_visable) {
            scoreCount++;
            console.log("score");
            localStorage.setItem("score", scoreCount);
            diamond1_visable = false;
        }
        if ((player.x >= diamondTwoX && player.x <= diamondTwoX + DIAMOND_DIMINTION) &&
            (player.y >= diamondTwoY && player.y <= diamondTwoY + DIAMOND_DIMINTION) &&
            diamond2_visable) {
            scoreCount++;
            console.log("score");
            localStorage.setItem("score", scoreCount);
            diamond2_visable = false;
        }


    }


}


function gameloop() {

    update();

    draw();
    movePlayer();
    if (keyDown) {
        moveEnemy();
        fireShoot();
    }

    window.requestAnimationFrame(gameloop);
}

window.requestAnimationFrame(gameloop);


window.addEventListener('keydown', input);
window.addEventListener('keyup', input);